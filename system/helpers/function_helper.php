<?php if ( ! defined('BASEPATH'))exit('No direct script access allowed');
	
	function objectToArray($d) {
		 if (is_object($d)) {
		  // Gets the properties of the given object
		  // with get_object_vars function
		  $d = get_object_vars($d);
		 }

		 if (is_array($d)) {
		  /*
		  * Return array converted to object
		  * Using __FUNCTION__ (Magic constant)
		  * for recursive call
		  */
		  return array_map(__FUNCTION__, $d);
		 }
		 else {
		  // Return array
		  return $d;
		 }
	}




	function orderListingsByasc($data, $field)
	{
		 $code = "return strnatcmp(\$a['$field'], \$b['$field']);"; 
		 usort($data, create_function('$a,$b', $code)); return $data;
	} 

	function orderListingsBydesc($data, $field) {
	   $code = "return strnatcmp(\$b['$field'], \$a['$field']);"; 
	   usort($data, create_function('$a,$b', $code)); return $data;
	}
	function validNumeric($val='')
	{
			return preg_match("/^[0-9]+$/",$val);
	}
	function arrayToObject($d) {
		 if (is_array($d)) {
		  /*
		  * Return array converted to object
		  * Using __FUNCTION__ (Magic constant)
		  * for recursive call
		  */
		  return (object) array_map(__FUNCTION__, $d);
		 }
		 else {
		  // Return object
		  return $d;
		 }
	}
	
        
        function encodeURL($id){
                return base64_encode($id*21);
        }
		
        function decodeURL($encodeid){
            $decodede=base64_decode($encodeid);
            return $decodede/21;
        }
        
        
        /*function to write data into txt file.*/
        
        function writetxt($query,$user_id){
                    
                $filepath=DOCUMENT_ROOT."/assets/topic_file/".$user_id.".txt";
                
                $handle = fopen($filepath, "a+");
                fwrite($handle, "\r\n".$query."\r\n");
                $handle1 = fopen($filepath, "r");
                $contents = fread($handle, filesize($filepath));
                $contents1 = fread($handle1, filesize($filepath));
            
                fclose($handle);
                return $filepath;
                //$txt=read_file_docx($filepath);
        }	
        
	function gendercount($arr)
	{//get gender count
		$returndata=getNewObject();
		$male=0;$female=0;
		if(isset($arr) && checkExists($arr))
		{
			foreach($arr as $val)
			{
				if($val->gender=='m')
					$male +=1;
				else if($val->gender=='f')
					$female +=1;
			}
		}
		$returndata->male=$male;
		$returndata->female=$female;
		return $returndata;
	}
	//end get gender count
	function resetLoginSession()
	{
		$_SESSION['ff'] = array();
	}
	
	function resetadminLoginSession()
	{
		$_SESSION['admin'] = array();
	}
	
	function checkLogin()
	{
		if(!isset($_SESSION['ff']->type) || $_SESSION['ff']->type!=DEVELOPER)
			redirect(SITE_PATH.'home/');
		else
			return $_SESSION['ff'];
	}
	
	function checkAdminLogin()
	{
		if(!isset($_SESSION['ff']->type) || $_SESSION['ff']->type!=ADMIN)
			redirect(SITE_PATH.'admin');
		else
			return true;
	}
	
	function getLoginDetails($redirect=true){//$_SESSION['ff']->userid
		if(isset($_SESSION['ff']) && checkExists($_SESSION['ff'])){
			return $_SESSION['ff'];
		}
		else if(isset($_SESSION['admin']) && checkExists($_SESSION['admin']))
		{
			return $_SESSION['admin'];
		}
	}
	
	function getNewObject()
	{
		return new stdClass;
	}
	
	function pr($request)
	{
		echo "<pre>";
		print_r($request);
		echo "</pre>";
	}
	
	
	
	
	function checkExists($val = '')
	{
		//added this as we using to initialize object with new stdClass from  this getNewObject(), thus when that empty object is passed for empty it return true(i.e exists value), so in order to avoid that mistake, just convert object to array and then when array will be check against for empty then it will return false(i.e no value exists)
		
		if(is_object($val) && in_array(strval(get_class($val)),array('stdClass')))
			$val = get_object_vars($val);
		
		if(is_string($val))
			$val=trim($val);
		
		if(empty($val) || is_null($val))
			return false;
		else
			return true;
	}	
	
	function calculatedays($datefrom='')
	{
		if(checkExists($datefrom))
			return (int)((getPhpTime()-$datefrom)/(24*60*60));
	}
	
	function getPhpTime($timetoconvert = '')
	{
		//dateTime class needs the date to be dd-mm-YYYY OR mm/dd/YYYY
		$timestamp='';
		if(phpversion()>'5.2.0')
		{
			//datetime class is introduces after 5.2 version only
			if(checkExists($timetoconvert))
			{
				$date = new DateTime($timetoconvert);
				if($date->format("Y")>=1970 && $date->format("Y")<2038)
				{
					$date->format("Y-m-d H:i:s");
					try{
						if(function_exists("getTimestamp"))
							$timestamp=$date->getTimestamp();
						else
							$timestamp=$date->format('U');
					}
					catch(Exception $e)
					{
						$timestamp=$date->format('U');
					}
					if($timestamp===FALSE)
						$timestamp=$date->format('U');
					
				}
				else
				{
					try{
						if(function_exists("getTimestamp"))
							$timestamp=$date->getTimestamp();
						else
							$timestamp=$date->format('U');
					}
					catch(Exception $e)
					{
						$timestamp=$date->format('U');
					}
					if($timestamp===FALSE)
						$timestamp=$date->format('U');
				}
			}
			else
			{
				$date = new DateTime();
				$date->format("Y-m-d H:i:s");
				try{
					if(function_exists("getTimestamp"))
						$timestamp=$date->getTimestamp();
					else
						$timestamp=$date->format('U');
				}
				catch(Exception $e)
				{
					$timestamp=$date->format('U');
				}
				if($timestamp===FALSE)
					$timestamp=$date->format('U');
			}
			
			return $timestamp;
		}
		else
		{
			//it will return the value only it the date range is between the php allowed timestamp range 1970-2038 and after beyond and prior to the range it will return false; so use above code to get the time stamp.
			if(checkExists($timetoconvert))
				return strtotime($timetoconvert);
			else
				return time();
		}
}


function newencrypt($text) 
{ 
    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, NUMBER, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
} 

function newdecrypt($text) 
{ 
    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, NUMBER, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))); 
} 


function checkPostRequest()
{
	if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST')
		return true;
	else 
		return false;
}


function generateUniqueString($length =8, $string ='')
{
	$possible ="bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ134689";
	$maxlength = strlen($possible);
	if($length > $maxlength)
		$length = $maxlength;
	$i = strlen($string); //if string is already passed then it will convert the string only to the required length
	while($i < $length)
	{
		$char = substr($possible, mt_rand(0, $maxlength-1),1);
		if(!strstr($string, $char))
		{
			$string.= $char;
			$i++;
		}
	}
	//trying to make the string as shuffled as possible that's y tried to shuffle it twice, so that the matching propablity will be the least
	$string = str_split($string);
	shuffle($string);
	$string = str_split(implode("",$string),3);
	shuffle($string);
	return implode("",$string); 
}

function recurse_copy($src,$dst)
{
	$dir = opendir($src);
	@mkdir($dst);
	while(false !==($file=readdir($dir)))
	{
		if(($file !='.') && ($file !='..'))
		{
			if(is_dir($src.'/'.$file))
				recurse_copy($src.'/'.$file,$dst.'/'.$file);
			else
				copy($src.'/'.$file,$dst.'/'.$file);
		}
	}
	closedir($dir);
	unlinkRecursive($src,1);
}

function unlinkRecursive($dir,$deleteRootToo)
{
	if(!$dh = @opendir($dir))
		return;
	while(false !== ($obj = readdir($dh)))
	{
		if($obj == '.' || $obj == '..')
			continue;
		
		if(!@unlink($dir.'/'.$obj))
			unlinkRecursive($dir.'/'.$obj, true);
	}
	closedir($dh);
	if($deleteRootToo)
		@rmdir($dir);
	
	return;
}



function encrypt($userid,$prefix)
{ 
  return $prefix.($userid*MULTIPLY);
}

function decrypt($encid,$prefix){
 $decid=$encid;
 $chkid=str_replace($prefix,'',$encid)/MULTIPLY;
 if(validNumeric($chkid))
  $decid=$chkid;
 return $decid;
}


function getPaginationString($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "&page=",$id)
{
$targetpage=$targetpage.'/';
	//defaults
	if(!$adjacents) $adjacents = 1;
	if(!$limit) $limit = 15;
	if(!$page) $page = 1;
	if(!$targetpage) $targetpage = "/";
	
	//other vars
	$prev = $page - 1;									//previous page is page - 1
	$next = $page + 1;									//next page is page + 1
	$lastpage = ceil($totalitems / $limit);				//lastpage is = total items / items per page, rounded up.
	$lpm1 = $lastpage - 1;								//last page minus 1
	
	if(!isset($margin))
	$margin= '';
	
	if(!isset($padding))
	$padding= '';
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"new_pagination\"";
		if($margin || $padding)
		{
			$pagination .= " style=\"";
			if($margin)
				$pagination .= "margin: $margin;";
			if($padding)
				$pagination .= "padding: $padding;";
			$pagination .= "\"";
		}
		$pagination .= ">";

		//previous button
		if ($page > 1) 
			$pagination .= "<a href=\"$targetpage$pagestring$prev\" class='yel_btn page'>&laquo; Prev</a>";
		else
			$pagination .= "<a href='javascript:void(0)' class='yel_btn page'><span class=\"disabled\" >&laquo; Prev</span></a>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination .= "<a href='javascript:void(0)' class='yel_btn page'><span class=\"current\" id=\"$counter\">$counter</span></a>";
					
				else
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\" class='yel_btn page'>$counter</a>";					
			}
		}
		elseif($lastpage >= 7 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 3))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination .= "<span class=\"current\" class='yel_btn page'>$counter</span>";
					else
						$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\" class='yel_btn page'>$counter</a>";					
				}
				$pagination .= "<span class=\"elipses\" class='yel_btn page'>...</span>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\" class='yel_btn page'>$lpm1</a>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\" class='yel_btn page'>$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination .= "<a href=\"" . $targetpage . $pagestring . "1\" class='yel_btn page'>1</a>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . "2\" class='yel_btn page'>2</a>";
				$pagination .= "<span class=\"elipses\" class='yel_btn page'>...</span>";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination .= "<span class=\"current\" class='yel_btn page'>$counter</span>";
					else
						$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\" class='yel_btn page'>$counter</a>";					
				}
				$pagination .= "...";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\" class='yel_btn page'>$lpm1</a>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\" class='yel_btn page'>$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination .= "<a href=\"" . $targetpage . $pagestring . "1\" class='yel_btn page'>1</a>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . "2\" class='yel_btn page'>2</a>";
				$pagination .= "<span class=\"elipses\">...</span>";
				for ($counter = $lastpage - (1 + ($adjacents * 3)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination .= "<span class=\"current\" class='yel_btn page'>$counter</span>";
					else
						$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\" class='yel_btn page'>$counter</a>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination .= "<a href=\"" . $targetpage . $pagestring . $next . "\" class='yel_btn page'>Next &raquo;</a>";
		else
			$pagination .= "<a href='javascript:void(0)' class='yel_btn page'><span class=\"disabled\">Next &raquo;</span></a>";
		$pagination .= "</div>\n";
	}
	
	return $pagination;

}

/*FILE UPLOADS FUNCTION*/
function createImageFolder($foldername = '',$upload_dir = ''){
	//if(!checkExists($foldername))
		//$foldername = date("Y").DS.date("m");
	
	$dirsuccesscreate=0;
	if(!checkExists($upload_dir))
		$upload_dir=PROFILE_IMG_PATH;
	
	if(@filetype($upload_dir)!='dir'){
		if (!mkdir($upload_dir,0777,true))//path does not exits. please check the path before adding files
			return false;
		else
		{
			$dirsuccesscreate=1;
			@chmod($upload_dir,0777);
		}
	}
	else //dir already exists
		$dirsuccesscreate=1;
	
	if($dirsuccesscreate){
		if(checkExists($foldername)){
			$requestdirpath=$upload_dir.$foldername;
			if(@filetype($requestdirpath)!='dir')
			{
				if (!mkdir($requestdirpath,0777,true))//path does not exits. please check the path before adding files
					return false;
				else{
					chmod($requestdirpath,0777);
					return true;
				}
			}
			else //dir already exists
				return true;
			
		}
	}
	return false;
	
}

function formatWithSuffix($input)
    {
        $suffixes = array('', 'k', 'm', 'g', 't');
        $suffixIndex = 0;
    
        while(abs($input) >= 1000 && $suffixIndex < sizeof($suffixes))
        {
            $suffixIndex++;
            $input /= 1000;
        }
    
        return (
            $input > 0
                // precision of 3 decimal places
                ? floor($input * 1000) / 1000
                : ceil($input * 1000) / 1000
            )
            . $suffixes[$suffixIndex];
    }
	
	
	
	function xtimeago($datetime,$type=''){
       // pr($datetime);
    $current_time = time();
    //pr($current_time);
    $timestamp = $datetime;
     //pr($timestamp);die;
     if(empty($timestamp)) {
      return "Bad date"; 
     }

        //type cast, current time, difference in timestamps
        $timestamp      = (int) $timestamp;

        $diff           = $current_time - $timestamp;

        //intervals in seconds
        $intervals      = array (
            'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
        );

       if($type =='minutes'){
          $diff = floor($diff/$intervals['minute']);
            return $diff;
       }

        //now we just find the difference
        if ($diff == 0)
        {
            return 'Just now';
        }   

      if ($diff < 10)
        {
            return 'Few sec ago';
        }  

        if ($diff < 60)
        {
            return $diff == 1 ? $diff . ' sec ago' : $diff . ' sec ago';
        }       

        if ($diff >= 60 && $diff < $intervals['hour'])
        {
            $diff = floor($diff/$intervals['minute']);
            return $diff == 1 ? $diff . ' min ago' : $diff . ' min ago';
        }       

        if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
        {
            $diff = floor($diff/$intervals['hour']);
            return $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
        }   

        if ($diff >= $intervals['day'] && $diff < $intervals['week'])
        {
            $diff = floor($diff/$intervals['day']);
            return $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
        }   

        if ($diff >= $intervals['week'] && $diff < $intervals['month'])
        {
            $diff = floor($diff/$intervals['week']);
            return $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
        }   

        if ($diff >= $intervals['month'] && $diff < $intervals['year'])
        {
            $diff = floor($diff/$intervals['month']);
            return $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
        }   

        if ($diff >= $intervals['year'])
        {
            $diff = floor($diff/$intervals['year']);
            return $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
        }
    }
 
?>
