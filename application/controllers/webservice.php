<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class webservice extends CI_Controller {
	 
	function __Construct(){
		parent::__Construct();
		$this->load->helper("function_helper");
		$this->load->model('Function_model');
		
        
	}
	
	function getResponse(){
		$response=getNewObject();
		$response->response=getNewObject();
		$content = file_get_contents("php://input");
		$requesttype = 'default';
		$requestdata ='';
		//$response->response->code = TRANSACTION_CODE_UNAUTH;
		if(!checkExists($content)){
			$postargs = $this->input->post(NULL,TRUE);
			$content =$postargs['data'];
		}
		
		if(!empty($content)){
			//pr($content);die;
			$requestdata=json_decode($content);

			if(isset($requestdata) && !empty($requestdata)){
				$requesttype=isset($requestdata->request->type)?$requestdata->request->type:'';
				//$devicetype=isset($requestdata->request->devicetype)?$requestdata->request->devicetype:'';
                // echo $devicetype;die;    
				    
                    
				switch($requesttype){
					case "categoryCaptions":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->categoryCaptions($requestinfo);
					break;

					case "getschoolList":
					    $response = $this->Function_model->getschoolList();
					break;

					case "otpLog":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->otpLog($requestinfo);
					break;

					case "getCustomGroupInfo":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->getCustomGroupInfo($requestinfo);
					break;

					case "getCustomGroupInfoExceptUser":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->getCustomGroupInfoExceptUser($requestinfo);
					break;
					
					case "help":
					    $response = $this->Function_model->help();
					break;
					
					case "userDevice":
					    $requestinfo=isset($requestdata->requestinfo)?($requestdata->requestinfo):'';
					    $response = $this->Function_model->userDevice($requestinfo);
					break;

					case "signUp":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->signUp($requestinfo);
					break;

					case "getRegisteredUsers":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->getRegisteredUsers($requestinfo);
					break;

					case "userInfo":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->userInfo($requestinfo);
					break;

					case "getGroupDetails":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->getGroupDetails($requestinfo);
					break;

					case "createCustomGroup":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->createCustomGroup($requestinfo);
					break;

					case "addGroupUser":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->addGroupUser($requestinfo);
					break;

					case "removeGroupUser":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->removeGroupUser($requestinfo);
					break;

					case "wayUpdate":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->wayUpdate($requestinfo);
					break;

					case "checkWay":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->checkWay($requestinfo);
					break;

					case "sendMailToOwner":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->sendMailToOwner($requestinfo);
					break;

					case "saveFiles":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->saveFiles($requestinfo);
					break;

					case "searchUser":
					    $requestinfo=isset($requestdata->requestinfo)?$requestdata->requestinfo:'';
					    $response = $this->Function_model->searchUser($requestinfo);
					break;
					
					default:
						$response->response->code=TRANSACTION_CODE_UNAUTH;
						$response->response->message="No such function is define.";
					break;
				}
			}else{
				$response->response->code=TRANSACTION_CODE_UNAUTH;
				$response->response->message="Unauthorized access**";
			}
		}else{
			$jsonerror='Invalid json Request';	
		}
		//echo $response->response->code;die;
		//pr($response);die;
		
		
		if(isset($jsonerror) && checkExists($jsonerror))
		$response->response->message=$jsonerror;
		$finalresponse = json_encode($response);
	
		echo $finalresponse; 
		

	}//getresponse
}
?>
