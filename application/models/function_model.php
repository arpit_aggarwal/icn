<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class function_model extends CI_Model
{
	function __Construct(){
		parent::__Construct();


	}



	function categoryCaptions($requestinfo){
		$response=getNewObject();
		$response->categoryCaptions=getNewObject();

		if(checkExists($requestinfo)){
			$user_id = isset($requestinfo->user_id)?$requestinfo->user_id:'';
			$active = 'N';

			$user_sql = "SELECT active FROM ".USERDEVICE." where user_id = ?";
			$q = $this->db->conn_id->prepare($user_sql);
			$q->bindValue(1, $user_id, PDO::PARAM_INT);
	        $q->execute();

			if($q->rowCount() > 0){
			$row = max($q->fetchAll(PDO::FETCH_ASSOC));
			$active = $row['active'];
		    }

		    $cat_array = array();

        	$sql = "SELECT `cat_id`,`display_value` FROM ".CATEGORY." WHERE `display_value`!='' and `is_active`='Y' and type='user'";
        	$cat_sql = $this->db->conn_id->prepare($sql);
        	$cat_sql->execute();

        	if($cat_sql->rowCount() > 0){
        		while($row = $cat_sql->fetch(PDO::FETCH_ASSOC)){
        			$caption_array = array();

        			$sql1 = "SELECT `cid`, `value`,`logic` FROM ".CATEGORYCAPTION." WHERE `value`!='' and `logic`!='' and `is_active`='Y' and `cat_id`=?";
        			$subcat_sql = $this->db->conn_id->prepare($sql1);
        			$subcat_sql->bindValue(1, $row['cat_id'], PDO::PARAM_INT);
        			$subcat_sql->execute();

        			if($subcat_sql->rowCount() > 0){
        			while($subrow = $subcat_sql->fetch(PDO::FETCH_ASSOC)){
        				$caption_array[] = array("caption_id"=>$subrow['cid'], "caption"=>$subrow['value'], "logic"=>$subrow['logic']);
        			}
					}        			

        			$cat_array[] = array("cat_id"=>$row['cat_id'], "category"=>$row['display_value'], "captions"=>$caption_array);
        		}

        		$response->categoryCaptions->responseCode=TRANSACTION_CODE_SUCCESS;
				$response->categoryCaptions->responseMessage=MESSAGE_SUCCESS;
				$response->categoryCaptions->responseData = $cat_array;
				$response->categoryCaptions->userActive = $active;
        	}else{
        		$response->categoryCaptions->responseCode=TRANSACTION_CODE_FAILED;
				$response->categoryCaptions->responseMessage=MESSAGE_FAILED;
				$response->categoryCaptions->userActive = $active;
        	}
		}

		return $response;
	}


	function getschoolList(){
	$response=getNewObject();
	$response->getschoolList=getNewObject();

		$sql = "SELECT * FROM schoollists";
		$q = $this->db->conn_id->prepare($sql);
        $q->execute();

		if($q->rowCount() > 0){
			while($row = $q->fetch(PDO::FETCH_ASSOC)){
				$id = $row['school_id'];
            	$schoolname = trim($row['schoolname']);

	            $school = str_replace('-', '', $schoolname);
	            $str17 = preg_replace('/[^A-Za-z0-9\-]/', '', $school);

	            $responseDataArray[] = array("id" => "$id", "schoolname" => "$schoolname", "searchstring" => "$str17");
			}

			$response->getschoolList->responseCode=TRANSACTION_CODE_SUCCESS;
			$response->getschoolList->responseMessage=MESSAGE_SUCCESS;
			$response->getschoolList->responseData = $responseDataArray;

	    }else{
	    	$response->getschoolList->responseCode=TRANSACTION_CODE_FAILED;
			$response->getschoolList->responseMessage=MESSAGE_FAILED;
	    }

		return $response;
	}


	function otpLog($requestinfo){
	$response=getNewObject();
	$response->otpLog=getNewObject();

		if(checkExists($requestinfo)){
			$user_id = $requestinfo->user_id;

			if($user_id!=''){

				$sql = "SELECT otp FROM ".OTPLOG." where user_id=? order by datetime desc limit 0,1";
				$otp_sql = $this->db->conn_id->prepare($sql);
    			$otp_sql->bindValue(1, $user_id, PDO::PARAM_INT);
    			$otp_sql->execute();

    			if($otp_sql->rowCount() > 0){
    				$row = $otp_sql->fetch(PDO::FETCH_ASSOC);
    				$otp = $row['otp'];

    				$response->otpLog->responseCode=TRANSACTION_CODE_SUCCESS;
					$response->otpLog->responseMessage=MESSAGE_SUCCESS;
					$response->otpLog->responseData = $otp;
    			}else{
    				$response->otpLog->responseCode=TRANSACTION_CODE_FAILED;
					$response->otpLog->responseMessage=MESSAGE_FAILED;
    			}

			}else{
				$response->otpLog->responseCode=TRANSACTION_CODE_FAILED;
				$response->otpLog->responseMessage=MESSAGE_FAILED;
			}
		}else{
			$response->otpLog->responseCode=TRANSACTION_CODE_FAILED;
			$response->otpLog->responseMessage=TRANSACTION_MSG_FAILED;
		}

		return $response;
	}


	function getCustomGroupInfo($requestinfo){
	$response=getNewObject();
	$response->getCustomGroupInfo=getNewObject();

		if(checkExists($requestinfo)){
			$user_id = $requestinfo->user_id;
			$pagecount = $requestinfo->page_count;  //15
		    $counter = $requestinfo->page_index;    //1    
		    $from = ($counter - 1) * $pagecount;

			if($user_id!=''){

				$sql = "SELECT * FROM ".CUSTOMGROUP." where IF(find_in_set (?, `user_id`),1,0)";
				$group_sql = $this->db->conn_id->prepare($sql);
    			$group_sql->bindValue(1, $user_id, PDO::PARAM_INT);
    			$group_sql->execute();

    			if($group_sql->rowCount() > 0){
    				while($row = $group_sql->fetch(PDO::FETCH_ASSOC)){
    					$g_id = $row['g_id'];
			            $userid = $row['user_id'];
			            $groupid = $row['groupid'];
			            $groupname = $row['groupname'];
			            $ownerid = $row['ownerid'];
			            $image = $row['image'];
			            $UniqueID = $row['UniqueID'];

			            $this->pdo = $this->load->database('db2', true);

			            $sql1 = "SELECT * FROM `ofMessageArchive` WHERE toJID = ?";
			            $msg_sql = $this->pdo->conn_id->prepare($sql1);
		    			$msg_sql->bindValue(1, $groupid, PDO::PARAM_INT);
		    			$msg_sql->execute();
			            $no_of_msg = $msg_sql->rowCount();

			            $user_id = trim($userid,",");


			            $last_msg = "";
			            $fromJID = "";
			            $sender_name = "";
			            $sql2 = "SELECT  `body`,fromJID FROM `ofMessageArchive` where toJID = ? order by `messageID` desc limit 0,1";
			            $lastmsg_sql = $this->pdo->conn_id->prepare($sql2);
		    			$lastmsg_sql->bindValue(1, $groupid, PDO::PARAM_INT);
		    			$lastmsg_sql->execute();
		    			if($lastmsg_sql->rowCount() > 0){
		    				$row1 = $lastmsg_sql->fetch(PDO::FETCH_ASSOC);
				            $body = $row1['body'];
				            $fromJID = $row1['fromJID'];
				            //echo $fromJID;die;
				            $body_json = json_decode($body);
				            $last_msg = $body_json->msg;
		    			}
		    			
		    			if($fromJID!=""){
		    				$user_sql = "SELECT name FROM ".USERLIST." WHERE user_id = '$fromJID'";
		    				$user_qry = $this->db->conn_id->prepare($user_sql);
		    				$user_qry->bindValue(1, $fromJID, PDO::PARAM_INT);
		    				$user_qry->execute();
		    				if($user_qry->rowCount() > 0){
		    					$user_res = $user_qry->fetch(PDO::FETCH_ASSOC);
		    					$sender_name = $user_res['name'];
		    				}
		    			}
			            

			            $res[] = array("GroupUniqueID" => $UniqueID, "user_id" => $userid, "group_id" => $groupid, "group_name" => $groupname, "owner_id" => $ownerid, "image" => $image, "No_of_msg" => $no_of_msg, "last_msg"=>$last_msg, "sender_name"=>$sender_name);

    				}
    					$responseDataArray = array_slice($res, $from, $pagecount);

    					$response->getCustomGroupInfo->responseCode=TRANSACTION_CODE_SUCCESS;
						$response->getCustomGroupInfo->responseMessage=MESSAGE_SUCCESS;
						$response->getCustomGroupInfo->responseData = $responseDataArray;
    			}else{
    				$response->getCustomGroupInfo->responseCode=TRANSACTION_CODE_FAILED;
					$response->getCustomGroupInfo->responseMessage="No group found for this user.";
    			}

			}else{
				$response->getCustomGroupInfo->responseCode=TRANSACTION_CODE_FAILED;
				$response->getCustomGroupInfo->responseMessage=MESSAGE_FAILED;
			}
		}else{
			$response->getCustomGroupInfo->responseCode=TRANSACTION_CODE_FAILED;
			$response->getCustomGroupInfo->responseMessage=TRANSACTION_MSG_FAILED;
		}

		return $response;
	}


	function getCustomGroupInfoExceptUser($requestinfo){
	$response=getNewObject();
	$response->getCustomGroupInfoExceptUser=getNewObject();

		if(checkExists($requestinfo)){
			$user_id = $requestinfo->user_id;
			$pagecount = $requestinfo->page_count;  //15
		    $counter = $requestinfo->page_index;    //1    
		    $from = ($counter - 1) * $pagecount;

			if($user_id!=''){

				$sql = "SELECT * FROM ".CUSTOMGROUP." where IF(find_in_set (?, `user_id`),0,1)";
				$group_sql = $this->db->conn_id->prepare($sql);
    			$group_sql->bindValue(1, $user_id, PDO::PARAM_INT);
    			$group_sql->execute();

    			if($group_sql->rowCount() > 0){
    				while($row = $group_sql->fetch(PDO::FETCH_ASSOC)){
    					$g_id = $row['g_id'];
			            $userid = $row['user_id'];
			            $groupid = $row['groupid'];
			            $groupname = $row['groupname'];
			            $ownerid = $row['ownerid'];
			            $image = $row['image'];
			            $UniqueID = $row['UniqueID'];

			            $this->pdo = $this->load->database('db2', true);

			            $sql1 = "SELECT * FROM `ofMessageArchive` WHERE toJID = ?";
			            $msg_sql = $this->pdo->conn_id->prepare($sql1);
		    			$msg_sql->bindValue(1, $groupid, PDO::PARAM_INT);
		    			$msg_sql->execute();
			            $no_of_msg = $msg_sql->rowCount();

			            $user_id = trim($userid,",");


			            $sql2 = "SELECT  `body` FROM `ofMessageArchive` where toJID = ? order by `messageID` desc limit 0,1";
			            $lastmsg_sql = $this->pdo->conn_id->prepare($sql2);
		    			$lastmsg_sql->bindValue(1, $groupid, PDO::PARAM_INT);
		    			$lastmsg_sql->execute();
		    			if($lastmsg_sql->rowCount() > 0){
		    				$row1 = $lastmsg_sql->fetch(PDO::FETCH_ASSOC);
				            $body = $row1['body'];
				            $body_json = json_decode($body);
				            $last_msg = $body_json->msg;
		    			}else{
			            	$last_msg = "";
		    			}
			            

			            $res[] = array("GroupUniqueID" => $UniqueID, "user_id" => $userid, "group_id" => $groupid, "group_name" => $groupname, "owner_id" => $ownerid, "image" => $image, "No_of_msg" => $no_of_msg, "last_msg"=>$last_msg);

    				}
    					$responseDataArray = array_slice($res, $from, $pagecount);

    					$response->getCustomGroupInfoExceptUser->responseCode=TRANSACTION_CODE_SUCCESS;
						$response->getCustomGroupInfoExceptUser->responseMessage=MESSAGE_SUCCESS;
						$response->getCustomGroupInfoExceptUser->responseData = $responseDataArray;
    			}else{
    				$response->getCustomGroupInfoExceptUser->responseCode=TRANSACTION_CODE_FAILED;
					$response->getCustomGroupInfoExceptUser->responseMessage="No group found except this user.";
    			}

			}else{
				$response->getCustomGroupInfoExceptUser->responseCode=TRANSACTION_CODE_FAILED;
				$response->getCustomGroupInfoExceptUser->responseMessage=MESSAGE_FAILED;
			}
		}else{
			$response->getCustomGroupInfoExceptUser->responseCode=TRANSACTION_CODE_FAILED;
			$response->getCustomGroupInfoExceptUser->responseMessage=TRANSACTION_MSG_FAILED;
		}

		return $response;
	}


	function help(){
	$response=getNewObject();
	$response->help=getNewObject();

		$sql = "SELECT * FROM ".USERLIST." where type='S' and user_id!=''";
		$help_sql = $this->db->conn_id->prepare($sql);
		$help_sql->execute();

		if($help_sql->rowCount() > 0){
			$row = $help_sql->fetch(PDO::FETCH_ASSOC);
	            
            $res = array("userid" => $row['user_id'], "mobile" => $row['mobilenumber'], "email" => $row['email']);

			$response->help->responseCode=TRANSACTION_CODE_SUCCESS;
			$response->help->responseMessage=MESSAGE_SUCCESS;
			$response->help->responseData = $res;
		}else{
			$response->help->responseCode=TRANSACTION_CODE_FAILED;
			$response->help->responseMessage="No support user found.";
		}


		return $response;
	}


	function userDevice($requestinfo){
	$response=getNewObject();
	$response->userDevice=getNewObject();

	if(checkExists($requestinfo)){

		$user_id = isset($requestinfo->user_id)?$requestinfo->user_id:'';
		$mobile = isset($requestinfo->mobile)?$requestinfo->mobile:'';
		$device_token = isset($requestinfo->device_token)?$requestinfo->device_token:'';
		$device_uniqueid = isset($requestinfo->device_uniqueid)?$requestinfo->device_uniqueid:'';
		$device_type = isset($requestinfo->device_type)?$requestinfo->device_type:'';
		$network_type = isset($requestinfo->network_type)?$requestinfo->network_type:'';
		$os = isset($requestinfo->os)?$requestinfo->os:'';
		$os_version = isset($requestinfo->os_version)?$requestinfo->os_version:'ios';
		$app_version = isset($requestinfo->app_version)?$requestinfo->app_version:'';
		$register_date = isset($requestinfo->register_date)?$requestinfo->register_date:'';
		$is_update = 0;
		$db_id = 0;
		$user_type='';

		/*if($mobile!=''){*/

			$sql = "SELECT * FROM ".USERDEVICE." WHERE `user_id`=?";
			$check_sql = $this->db->conn_id->prepare($sql);
			$check_sql->bindValue(1, $user_id, PDO::PARAM_INT);
			$check_sql->execute();

			if($check_sql->rowCount() > 0){
				$sql1 = "UPDATE ".USERDEVICE." SET `mobile` = ?,`device_token` = ?,`device_uniqueid` = ?,`device_type` = ?,`network_type` = ?,`os` = ?,`os_version` = ?,`app_version` = ?  WHERE `user_id` = ?";
				$update_sql = $this->db->conn_id->prepare($sql1);
				$update_sql->bindValue(1, $mobile, PDO::PARAM_INT);
				$update_sql->bindValue(2, $device_token, PDO::PARAM_INT);
				$update_sql->bindValue(3, $device_uniqueid, PDO::PARAM_INT);
				$update_sql->bindValue(4, $device_type, PDO::PARAM_INT);
				$update_sql->bindValue(5, $network_type, PDO::PARAM_INT);
				$update_sql->bindValue(6, $os, PDO::PARAM_INT);
				$update_sql->bindValue(7, $os_version, PDO::PARAM_INT);
				$update_sql->bindValue(8, $app_version, PDO::PARAM_INT);
				$update_sql->bindValue(9, $user_id, PDO::PARAM_INT);
				$update_sql->execute();
                

			}else{

				$sql3 = "INSERT INTO ".USERDEVICE." (`user_id`, `mobile`, `device_token`, `device_uniqueid`, `device_type`, `network_type`, `os`, `os_version`, `app_version`, `register_date`) VALUES (?,?,?,?,?,?,?,?,?,?)";
				$insert_sql = $this->db->conn_id->prepare($sql3);
				$insert_sql->bindValue(1, $user_id, PDO::PARAM_INT);
				$insert_sql->bindValue(2, $mobile, PDO::PARAM_INT);
				$insert_sql->bindValue(3, $device_token, PDO::PARAM_INT);
				$insert_sql->bindValue(4, $device_uniqueid, PDO::PARAM_INT);
				$insert_sql->bindValue(5, $device_type, PDO::PARAM_INT);
				$insert_sql->bindValue(6, $network_type, PDO::PARAM_INT);
				$insert_sql->bindValue(7, $os, PDO::PARAM_INT);
				$insert_sql->bindValue(8, $os_version, PDO::PARAM_INT);
				$insert_sql->bindValue(9, $app_version, PDO::PARAM_INT);
				$insert_sql->bindValue(10, $register_date, PDO::PARAM_INT);
				$insert_sql->execute();
			}

			$sql2 = "SELECT id FROM ".USERDEVICE." WHERE `user_id`=?";
			$id_sql = $this->db->conn_id->prepare($sql2);
			$id_sql->bindValue(1, $user_id, PDO::PARAM_INT);
			$id_sql->execute();
			if($id_sql->rowCount() > 0){
				$row = $id_sql->fetch(PDO::FETCH_ASSOC);
				$db_id = $row['id'];
			}

			$sql4 = "SELECT type FROM ".USERLIST." WHERE `user_id`=?";
			$type_sql = $this->db->conn_id->prepare($sql4);
			$type_sql->bindValue(1, $user_id, PDO::PARAM_INT);
			$type_sql->execute();
			if($type_sql->rowCount() > 0){
				$row = $type_sql->fetch(PDO::FETCH_ASSOC);
				$user_type = $row['type'];
				$is_update=1;
			}

			if($db_id > 0){
                /*$otp = mt_rand(100000, 999999);

                $input_xml="<message-submit-request><username>trinitymobile</username><password>123456</password><sender-id>TRIAPP</sender-id><MType>TXT</MType><message-text><text>ICN OTP: $otp</text><to>$mobile</to></message-text></message-submit-request>";
                //echo $input_xml;die;

                $url = "http://www.smsjust.com/sms/user/XMLAPI/send.php";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                // Following line is compulsary to add as it is:
                curl_setopt($ch, CURLOPT_POSTFIELDS,"data=" . $input_xml);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
                $data = curl_exec($ch);
                curl_close($ch);
                //echo $data;die;
                $result = strrchr($data,"ES1");*/
                //print_r($result);die;

                /*if(!empty($data)){
                	$sql4 = "INSERT INTO ".OTPLOG." (`user_id`, `otp`) VALUES (?,?)";
					$otp_sql = $this->db->conn_id->prepare($sql4);
					$otp_sql->bindValue(1, $user_id, PDO::PARAM_INT);
					$otp_sql->bindValue(2, $otp, PDO::PARAM_INT);
					$otp_sql->execute();

                    if($otp_sql){*/
                        $response->userDevice->responseCode=TRANSACTION_CODE_SUCCESS;
						$response->userDevice->responseMessage=MESSAGE_SUCCESS;
						$response->userDevice->is_update=$is_update;
						$response->userDevice->user_type=$user_type;
                    /*}else{
                       	$response->userDevice->responseCode=TRANSACTION_CODE_FAILED;
						$response->userDevice->responseMessage="OTP data not successfully inserted.";
						$response->userDevice->is_update=$is_update;
						$response->userDevice->user_type=$user_type;
                    }*/

                /*}else{
                	$response->userDevice->responseCode=TRANSACTION_CODE_FAILED;
					$response->userDevice->responseMessage="OTP not send.";
					$response->userDevice->is_update=$is_update;
					$response->userDevice->user_type=$user_type;
                }*/
            }else{
            	$response->userDevice->responseCode=TRANSACTION_CODE_FAILED;
				$response->userDevice->responseMessage=MESSAGE_FAILED;
				$response->userDevice->is_update=$is_update;
				$response->userDevice->user_type=$user_type;
            }



/*		}else{
			$response->userDevice->responseCode=TRANSACTION_CODE_FAILED;
			$response->userDevice->responseMessage="Mobile number not found.";
		}*/


	}else{
		$response->userDevice->responseCode=TRANSACTION_CODE_FAILED;
		$response->userDevice->responseMessage="No request data found.";
	}
	return $response;
	}


	function signUp($requestinfo){
	$response=getNewObject();
	$response->signUp=getNewObject();

	if(checkExists($requestinfo)){

		$yourname = isset($requestinfo->yourname)?$requestinfo->yourname:'';
		$phonenumber = isset($requestinfo->phonenumber)?$requestinfo->phonenumber:'';
		$emailid = isset($requestinfo->emailid)?$requestinfo->emailid:'';
		$user_id = isset($requestinfo->user_id)?$requestinfo->user_id:'';
		$status = isset($requestinfo->status)?$requestinfo->status:'';
		$fb_id = isset($requestinfo->fb_id)?$requestinfo->fb_id:'';
		$gplus_id = isset($requestinfo->gplus_id)?$requestinfo->gplus_id:'';
		$type = isset($requestinfo->type)?$requestinfo->type:'';
		$image = isset($requestinfo->image)?$requestinfo->image:'';
		$childArray = isset($requestinfo->childArray)?$requestinfo->childArray:'';
		

		if($user_id!=''){
			$FileName='';
			if(isset($image) && ($image)!=''){
			$thefile = base64_decode($image);
			$user_name = explode('@',$user_id);
			$user = $user_name[0];
			$FileName=$user . ".JPG";
			file_put_contents(IMAGE_URL.$FileName, $thefile);
			}

			$sql = "SELECT * FROM ".USERLIST." WHERE `user_id`=?";
			$check_sql = $this->db->conn_id->prepare($sql);
			$check_sql->bindValue(1, $user_id, PDO::PARAM_INT);
			$check_sql->execute();
			$error_aary = array();

			if($check_sql->rowCount() > 0){
				$sql1 = "UPDATE ".USERLIST." SET `name`=?,`mobilenumber`=?,`email`=?,`image`=?,`status`=?,`fb_id`=?,`gplus_id`=?,`type`=? WHERE user_id=?";
				$update_sql = $this->db->conn_id->prepare($sql1);
				$update_sql->bindValue(1, $yourname, PDO::PARAM_INT);
				$update_sql->bindValue(2, $phonenumber, PDO::PARAM_INT);
				$update_sql->bindValue(3, $emailid, PDO::PARAM_INT);
				$update_sql->bindValue(4, IMAGE_SAVE_URL.$FileName, PDO::PARAM_INT);
				$update_sql->bindValue(5, $status, PDO::PARAM_INT);
				$update_sql->bindValue(6, $fb_id, PDO::PARAM_INT);
				$update_sql->bindValue(7, $gplus_id, PDO::PARAM_INT);
				$update_sql->bindValue(8, $type, PDO::PARAM_INT);
				$update_sql->bindValue(9, $user_id, PDO::PARAM_INT);
				$update_sql->execute();

                $sql2 = "DELETE FROM ".CHILDLIST." WHERE `user_id`=?";
                $delete_sql = $this->db->conn_id->prepare($sql2);
                $delete_sql->bindValue(1, $user_id, PDO::PARAM_INT);
                $delete_sql->execute();

                if(!empty($childArray)){

                	foreach ($childArray as $data){
                        
                    $childname = $data->childname;
                    $school_id = $data->school_id;
                    $team_code = $data->team_code;

                    $child_sql = "INSERT INTO ".CHILDLIST." (`user_id`, `childname`, `school_id`, `team_code`) VALUES (?,?,?,?)";
                    $child_insert_sql = $this->db->conn_id->prepare($child_sql);
					$child_insert_sql->bindValue(1, $user_id, PDO::PARAM_INT);
					$child_insert_sql->bindValue(2, $childname, PDO::PARAM_INT);
					$child_insert_sql->bindValue(3, $school_id, PDO::PARAM_INT);
					$child_insert_sql->bindValue(4, $team_code, PDO::PARAM_INT);
					$child_insert_sql->execute();

					$group_sql = "SELECT * from ".CUSTOMGROUP." where UniqueID = ?";
					$group_qry = $this->db->conn_id->prepare($group_sql);
					$group_qry->bindValue(1, $team_code, PDO::PARAM_INT);
					$group_qry->execute();
					if($group_qry->rowCount() > 0){
						$row = $group_qry->fetch(PDO::FETCH_ASSOC);
                        $user_ids = $row['user_id'];
                        $groupid = $row['groupid'];
                        $groupname = $row['groupname'];
                        $ownerid = $row['ownerid'];

                        $pieces = explode(",", $user_ids);
		                //$key=array_search($user_id, $pieces);
		                
		                if(!in_array($user_id, $pieces)){
		                	//$group = str_replace(' ', '$$', $row['groupname']);

	                        $group = explode('@', $groupid);
	                        $groupids = $group[0];

	                        $owner = explode('@', $ownerid);
	                        $ownerid = $owner[0];

	                        $userids = $user_ids.",".$user_id;

	                        $open = $this->creategroupmember($groupname,$groupids,$ownerid,$userids);

			                $sql1 = "UPDATE ".CUSTOMGROUP." SET `user_id`=? WHERE groupid=?";
			                $insert_sql = $this->db->conn_id->prepare($sql1);
			                $insert_sql->bindValue(1, $userids, PDO::PARAM_INT);
			                $insert_sql->bindValue(2, $groupid, PDO::PARAM_INT);
			                $insert_sql->execute();
		                }

                        
					}else{
						$error_aary[] = array("error"=>"No group found with ID ".$team_code);
					}

                	}
                }

                if($update_sql){
                	$response->signUp->responseCode=TRANSACTION_CODE_SUCCESS;
					$response->signUp->responseMessage="User data updated successfully.";
					$response->signUp->responseError=$error_aary;
                }else{
                	$response->signUp->responseCode=TRANSACTION_CODE_FAILED;
					$response->signUp->responseMessage="User data not updated.";
					$response->signUp->responseError=$error_aary;
                }
			}else{

				$sql3 = "INSERT INTO ".USERLIST." (`name`, `mobilenumber`, `user_id`, `email`, `image`, `status`, `fb_id`, `gplus_id`, `type`) VALUES (?,?,?,?,?,?,?,?,?)";
				$insert_sql = $this->db->conn_id->prepare($sql3);
				$insert_sql->bindValue(1, $yourname, PDO::PARAM_INT);
				$insert_sql->bindValue(2, $phonenumber, PDO::PARAM_INT);
				$insert_sql->bindValue(3, $user_id, PDO::PARAM_INT);
				$insert_sql->bindValue(4, $emailid, PDO::PARAM_INT);
				$insert_sql->bindValue(5, IMAGE_SAVE_URL.$FileName, PDO::PARAM_INT);
				$insert_sql->bindValue(6, $status, PDO::PARAM_INT);
				$insert_sql->bindValue(7, $fb_id, PDO::PARAM_INT);
				$insert_sql->bindValue(8, $gplus_id, PDO::PARAM_INT);
				$insert_sql->bindValue(9, $type, PDO::PARAM_INT);
				$insert_sql->execute();

                if(!empty($childArray)){
                	
                	foreach ($childArray as $data){
                        
                    $childname = $data->childname;
                    $school_id = $data->school_id;
                    $team_code = $data->team_code;

                    $child_sql = "INSERT INTO ".CHILDLIST." (`user_id`, `childname`, `school_id`, `team_code`) VALUES (?,?,?,?)";
                    $child_insert_sql = $this->db->conn_id->prepare($child_sql);
					$child_insert_sql->bindValue(1, $user_id, PDO::PARAM_INT);
					$child_insert_sql->bindValue(2, $childname, PDO::PARAM_INT);
					$child_insert_sql->bindValue(3, $school_id, PDO::PARAM_INT);
					$child_insert_sql->bindValue(4, $team_code, PDO::PARAM_INT);
					$child_insert_sql->execute();

					$group_sql = "SELECT * from ".CUSTOMGROUP." where UniqueID = ?";
					$group_qry = $this->db->conn_id->prepare($group_sql);
					$group_qry->bindValue(1, $team_code, PDO::PARAM_INT);
					$group_qry->execute();
					if($group_qry->rowCount() > 0){
						$row = $group_qry->fetch(PDO::FETCH_ASSOC);
                        $user_ids = $row['user_id'];
                        $groupid = $row['groupid'];
                        $groupname = $row['groupname'];
                        $ownerid = $row['ownerid'];

                        $pieces = explode(",", $user_ids);
		                //$key=array_search($user_id, $pieces);
		                
		                if(!in_array($user_id, $pieces)){
		                	//$group = str_replace(' ', '$$', $row['groupname']);

	                        $group = explode('@', $groupid);
	                        $groupids = $group[0];

	                        $owner = explode('@', $ownerid);
	                        $ownerid = $owner[0];

	                        $userids = $user_ids.",".$user_id;

	                        $open = $this->creategroupmember($groupname,$groupids,$ownerid,$userids);

	                        $sql1 = "UPDATE ".CUSTOMGROUP." SET `user_id`=? WHERE groupid=?";
			                $insert_sql = $this->db->conn_id->prepare($sql1);
			                $insert_sql->bindValue(1, $userids, PDO::PARAM_INT);
			                $insert_sql->bindValue(2, $groupid, PDO::PARAM_INT);
			                $insert_sql->execute();
		                }

                        
					}else{
						$error_aary[] = array("error"=>"No group found with ID ".$team_code);
					}

                	}
                }

                if($insert_sql){
                	$response->signUp->responseCode=TRANSACTION_CODE_SUCCESS;
					$response->signUp->responseMessage="User data inserted successfully.";
					$response->signUp->responseError=$error_aary;
                }else{
                	$response->signUp->responseCode=TRANSACTION_CODE_FAILED;
					$response->signUp->responseMessage="User data not inserted.";
					$response->signUp->responseError=$error_aary;
                }
			}

		}else{
			$response->signUp->responseCode=TRANSACTION_CODE_FAILED;
			$response->signUp->responseMessage="UserID is blank.";
		}


	}else{
		$response->signUp->responseCode=TRANSACTION_CODE_FAILED;
		$response->signUp->responseMessage="No request data found.";
	}
	return $response;
	}


	function getRegisteredUsers($requestinfo){
	$response=getNewObject();
	$response->getRegisteredUsers=getNewObject();

	if(checkExists($requestinfo)){

		$user_id = isset($requestinfo->user_id)?$requestinfo->user_id:'';
		

		if($user_id!=''){
			
			$sql = "SELECT `name`,`mobilenumber`,`user_id`,`image`,`type` FROM ".USERLIST." WHERE `user_id`!=? and type!='S'";
			$user_sql = $this->db->conn_id->prepare($sql);
			$user_sql->bindValue(1, $user_id, PDO::PARAM_INT);
			$user_sql->execute();

			if($user_sql->rowCount() > 0){
				$responseDataArray = array();
				while($row = $user_sql->fetch(PDO::FETCH_ASSOC)){

					$child_array = array();
					$sql1 = "SELECT * FROM ".CHILDLIST." WHERE `user_id`=?";
						$child_sql = $this->db->conn_id->prepare($sql1);
						$child_sql->bindValue(1, $row['user_id'], PDO::PARAM_INT);
						$child_sql->execute();
						
						if($child_sql->rowCount()>0){
							while($child_row = $child_sql->fetch(PDO::FETCH_ASSOC)){
								$child_array[] = array("Name"=>$child_row['childname'], "schoolID"=>$child_row['school_id'], "teamCode"=>$child_row['team_code']);
							}
						}


					$responseDataArray[] = array("user_id"=>$row['user_id'], "mobile"=>$row['mobilenumber'], "name"=>$row['name'], "image"=>$row['image'],"type"=>$row['type'], "childArray"=>$child_array);
				}
				$response->getRegisteredUsers->responseCode=TRANSACTION_CODE_SUCCESS;
				$response->getRegisteredUsers->responseMessage=MESSAGE_SUCCESS;
				$response->getRegisteredUsers->responseData=$responseDataArray;
			}else{
				$response->getRegisteredUsers->responseCode=TRANSACTION_CODE_FAILED;
				$response->getRegisteredUsers->responseMessage="No Users found.";
			}


		}else{
			$response->getRegisteredUsers->responseCode=TRANSACTION_CODE_FAILED;
			$response->getRegisteredUsers->responseMessage="UserID is blank.";
		}


	}else{
		$response->getRegisteredUsers->responseCode=TRANSACTION_CODE_FAILED;
		$response->getRegisteredUsers->responseMessage="No request data found.";
	}
	return $response;
	}


	function userInfo($requestinfo){
	$response=getNewObject();
	$response->userInfo=getNewObject();

	if(checkExists($requestinfo)){

		$user_id = isset($requestinfo->user_id)?$requestinfo->user_id:'';
		

		if($user_id!=''){
			
			$sql = "SELECT * FROM ".USERLIST." WHERE `user_id`=?";
			$user_sql = $this->db->conn_id->prepare($sql);
			$user_sql->bindValue(1, $user_id, PDO::PARAM_INT);
			$user_sql->execute();

			if($user_sql->rowCount() > 0){
				$row = $user_sql->fetch(PDO::FETCH_ASSOC);

				if($row['type'] == 'parent'){
					$sql1 = "SELECT * FROM ".CHILDLIST." WHERE `user_id`=?";
					$child_sql = $this->db->conn_id->prepare($sql1);
					$child_sql->bindValue(1, $user_id, PDO::PARAM_INT);
					$child_sql->execute();
					$child_array = array();

					if($child_sql->rowCount() > 0){
						while($child_row = $child_sql->fetch(PDO::FETCH_ASSOC)){
							$child_array[] = array("Name"=>$child_row['childname'], "schoolID"=>$child_row['school_id'], "teamCode"=>$child_row['team_code']);
						}
					}
					$responseDataArray = array("Name"=>$row['name'], "Mobile"=>$row['mobilenumber'], "Email"=>$row['email'], "Image"=>$row['image'], "fb_id"=>$row['fb_id'], "gplus_id"=>$row['gplus_id'], "type"=>$row['type'], "childInfo"=>$child_array);
				}else{
					$responseDataArray = array("Name"=>$row['name'], "Mobile"=>$row['mobilenumber'], "Email"=>$row['email'], "Image"=>$row['image'], "fb_id"=>$row['fb_id'], "gplus_id"=>$row['gplus_id'], "type"=>$row['type']);
				}

				$response->userInfo->responseCode=TRANSACTION_CODE_SUCCESS;
				$response->userInfo->responseMessage=MESSAGE_SUCCESS;
				$response->userInfo->responseData=$responseDataArray;
			}else{
				$response->userInfo->responseCode=TRANSACTION_CODE_FAILED;
				$response->userInfo->responseMessage="No Users found.";
			}


		}else{
			$response->userInfo->responseCode=TRANSACTION_CODE_FAILED;
			$response->userInfo->responseMessage="UserID is blank.";
		}


	}else{
		$response->userInfo->responseCode=TRANSACTION_CODE_FAILED;
		$response->userInfo->responseMessage="No request data found.";
	}
	return $response;
	}


	function getGroupDetails($requestinfo){
	$response=getNewObject();
	$response->getGroupDetails=getNewObject();

	if(checkExists($requestinfo)){

		$group_id = isset($requestinfo->group_id)?$requestinfo->group_id:'';
		

		if($group_id!=''){
			
			$sql = "SELECT * FROM ".CUSTOMGROUP." WHERE `groupid`=?";
			$group_sql = $this->db->conn_id->prepare($sql);
			$group_sql->bindValue(1, $group_id, PDO::PARAM_INT);
			$group_sql->execute();

			if($group_sql->rowCount() > 0){
				$row = $group_sql->fetch(PDO::FETCH_ASSOC);

				$userids = explode(',',$row['user_id']);
				$user_array = array();
				foreach($userids as $ids){

					$sql1 = "SELECT * FROM ".USERLIST." WHERE `user_id`=?";
					$user_sql = $this->db->conn_id->prepare($sql1);
					$user_sql->bindValue(1, $ids, PDO::PARAM_INT);
					$user_sql->execute();
					$child_array = array();

					if($user_sql->rowCount() > 0){
						$user_rwo = $user_sql->fetch(PDO::FETCH_ASSOC);

						if($user_rwo['type'] == 'parent'){
							$sql1 = "SELECT * FROM ".CHILDLIST." WHERE `user_id`=?";
							$child_sql = $this->db->conn_id->prepare($sql1);
							$child_sql->bindValue(1, $ids, PDO::PARAM_INT);
							$child_sql->execute();
							
							if($child_sql->rowCount()>0){
								while($child_row = $child_sql->fetch(PDO::FETCH_ASSOC)){
									$child_array[] = array("Name"=>$child_row['childname'], "schoolID"=>$child_row['school_id'], "teamCode"=>$child_row['team_code']);
								}
							}
						}

						$user_array[] = array("user_id"=>$ids,"Name"=>$user_rwo['name'], "Mobile"=>$user_rwo['mobilenumber'], "Email"=>$user_rwo['email'], "Image"=>$user_rwo['image'], "type"=>$user_rwo['type'], "childInfo"=>$child_array);

					}

				}

				$responseDataArray = array("GroupUniqueID" => $row['UniqueID'], "user_id" => $row['user_id'], "group_id" => $group_id, "group_name" => $row['groupname'], "owner_id" => $row['ownerid'], "image" => $row['image'], "GroupUserDetails"=>$user_array);
				

				$response->getGroupDetails->responseCode=TRANSACTION_CODE_SUCCESS;
				$response->getGroupDetails->responseMessage=MESSAGE_SUCCESS;
				$response->getGroupDetails->responseData=$responseDataArray;
			}else{
				$response->getGroupDetails->responseCode=TRANSACTION_CODE_FAILED;
				$response->getGroupDetails->responseMessage="No Group found.";
			}

		}else{
			$response->getGroupDetails->responseCode=TRANSACTION_CODE_FAILED;
			$response->getGroupDetails->responseMessage="GroupID is blank.";
		}

	}else{
		$response->getGroupDetails->responseCode=TRANSACTION_CODE_FAILED;
		$response->getGroupDetails->responseMessage="No request data found.";
	}
	return $response;
	}

	function createCustomGroup($requestinfo){
		$response=getNewObject();
		$response->createCustomGroup=getNewObject();

		if(checkExists($requestinfo)){

			$groupid = isset($requestinfo->groupid)?$requestinfo->groupid:'';
			$type = isset($requestinfo->type)?$requestinfo->type:'';
			$userids = isset($requestinfo->userids)?$requestinfo->userids:'';
			$groupname = isset($requestinfo->groupname)?$requestinfo->groupname:'';
			$ownerids = isset($requestinfo->ownerid)?$requestinfo->ownerid:'';
			$image = isset($requestinfo->image)?$requestinfo->image:'';

            $group = explode('@', $groupid);
            $groupids = $group[0];

            //$group = str_replace(' ', '$$', $groupname);

            $sql = "SELECT g_id FROM ".CUSTOMGROUP." where groupid=?";
            $chk_sql = $this->db->conn_id->prepare($sql);
            $chk_sql->bindValue(1, $groupid, PDO::PARAM_INT);
            $chk_sql->execute();

            if($chk_sql->rowCount() == 0){
            	$FileName='';
				if(isset($image) && ($image)!=''){
				$thefile = base64_decode($image);
				$FileName=$groupids . ".JPG";
				file_put_contents(IMAGE_URL.$FileName, $thefile);
				}

                $group_first = substr($groupname,0,1);
                $group_last = substr($groupname,-1);

                $final_group_sub = $group_first.$group_last;
                $final_group_sub = strtoupper($final_group_sub);

                $owner = explode('@', $ownerids);
                $ownerid = $owner[0];

                $owner_num = explode('icn',$ownerid);
                $owner_num = $owner_num[0];

                $owner_num_first_two = substr($owner_num,0,2);
                $owner_num_last_two = substr($owner_num,-2);

                $unique_id = $owner_num_first_two.$final_group_sub.$owner_num_last_two;

                $sql1 = "INSERT INTO `customgroup`(`UniqueID`, `user_id`, `groupid`, `groupname`, `ownerid`, `image`) VALUES (?,?,?,?,?,?)";
                $insert_sql = $this->db->conn_id->prepare($sql1);
                $insert_sql->bindValue(1, $unique_id, PDO::PARAM_INT);
                $insert_sql->bindValue(2, $userids, PDO::PARAM_INT);
                $insert_sql->bindValue(3, $groupid, PDO::PARAM_INT);
                $insert_sql->bindValue(4, $groupname, PDO::PARAM_INT);
                $insert_sql->bindValue(5, $ownerids, PDO::PARAM_INT);
                $insert_sql->bindValue(6, IMAGE_SAVE_URL.$FileName, PDO::PARAM_INT);
                $insert_sql->execute();

                $open = $this->creategroupmember($groupname,$groupids,$ownerid,$userids);

                 if ($insert_sql) {

                    $respose = array("uniqueGroupID"=>$unique_id, "userid" => $userids, "groupname" => $groupname, "groupid" => $groupid, "imageurl" => IMAGE_SAVE_URL . $FileName);

                    $response->createCustomGroup->responseCode=TRANSACTION_CODE_SUCCESS;
					$response->createCustomGroup->responseMessage=MESSAGE_SUCCESS;
					$response->createCustomGroup->responseData=$respose;
                } else {

                    $response->createCustomGroup->responseCode=TRANSACTION_CODE_FAILED;
					$response->createCustomGroup->responseMessage="Group not created.";
                }

            }else{
            	$response->createCustomGroup->responseCode=TRANSACTION_CODE_FAILED;
				$response->createCustomGroup->responseMessage="Group Already Exist.";
            }



		}else{
		$response->createCustomGroup->responseCode=TRANSACTION_CODE_FAILED;
		$response->createCustomGroup->responseMessage="No request data found.";
	}
	return $response;
	}

	function addGroupUser($requestinfo){
		$response=getNewObject();
		$response->addGroupUser=getNewObject();

		if(checkExists($requestinfo)){

			$team_code = isset($requestinfo->team_code)?$requestinfo->team_code:'';
			$userid = isset($requestinfo->userid)?$requestinfo->userid:'';

            $sql = "SELECT user_id,ownerid,groupname,groupid FROM ".CUSTOMGROUP." WHERE UniqueID=?";
            $chk_sql = $this->db->conn_id->prepare($sql);
            $chk_sql->bindValue(1, $team_code, PDO::PARAM_INT);
            $chk_sql->execute();

            if($chk_sql->rowCount() > 0){

            	$user_id_add = explode(',', $userid);


            	foreach($user_id_add as $user_to_add){

            	$sql = "SELECT user_id,ownerid,groupname,groupid FROM ".CUSTOMGROUP." WHERE UniqueID=?";
	            $chk_sql = $this->db->conn_id->prepare($sql);
	            $chk_sql->bindValue(1, $team_code, PDO::PARAM_INT);
	            $chk_sql->execute();
	            if($chk_sql->rowCount() > 0){
	            	$row = $chk_sql->fetch(PDO::FETCH_ASSOC);
	            	$user_ids = $row['user_id'];
	            	//echo $user_ids;die;
	            	$ownerids = $row['ownerid'];
	            	$groupid = $row['groupid'];

	            	$group = explode('@', $groupid);
	            	$groupids = $group[0];

	            	$pieces = explode(",", $user_ids);
	            }

                if(!in_array($user_to_add, $pieces))
                {
                	//echo $user_to_add;die;
                	

                    $userids = $user_ids.",".$user_to_add;
                    //echo $userids;die;
	            	//$group = str_replace(' ', '$$', $row['groupname']);

	            	$owner = explode('@', $ownerids);
	                $ownerid = $owner[0];

	                $sql1 = "UPDATE ".CUSTOMGROUP." SET `user_id`=? WHERE UniqueID=?";
	                $insert_sql = $this->db->conn_id->prepare($sql1);
	                $insert_sql->bindValue(1, $userids, PDO::PARAM_INT);
	                $insert_sql->bindValue(2, $team_code, PDO::PARAM_INT);
	                $insert_sql->execute();

	                $open = $this->creategroupmember($row['groupname'],$groupids,$ownerid,$userids);
	                //print_r($open);die;
	                if ($insert_sql) {

	                    $respose = array("userid" => $userids, "groupname" => $row['groupname'], "groupid" => $groupid);

	                    $response->addGroupUser->responseCode=TRANSACTION_CODE_SUCCESS;
						$response->addGroupUser->responseMessage=MESSAGE_SUCCESS;
						$response->addGroupUser->responseData=$respose;
	                } else {

	                    $response->addGroupUser->responseCode=TRANSACTION_CODE_FAILED;
						$response->addGroupUser->responseMessage="User not added.";
	                }
                }else{
                	$response->addGroupUser->responseCode=TRANSACTION_CODE_FAILED;
					$response->addGroupUser->responseMessage="User already added.";
                }
            	}

            	
            }else{
            	$response->addGroupUser->responseCode=TRANSACTION_CODE_FAILED;
				$response->addGroupUser->responseMessage="No Group found.";
            }

		}else{
		$response->addGroupUser->responseCode=TRANSACTION_CODE_FAILED;
		$response->addGroupUser->responseMessage="No request data found.";
	}
	return $response;
	}


	function removeGroupUser($requestinfo){
		$response=getNewObject();
		$response->removeGroupUser=getNewObject();

		if(checkExists($requestinfo)){

			$groupid = isset($requestinfo->groupid)?$requestinfo->groupid:'';
			$userid = isset($requestinfo->userid)?$requestinfo->userid:'';

            $group = explode('@', $groupid);
            $groupids = $group[0];

            $sql = "SELECT user_id,ownerid,groupname FROM ".CUSTOMGROUP." WHERE groupid=?";
            $chk_sql = $this->db->conn_id->prepare($sql);
            $chk_sql->bindValue(1, $groupid, PDO::PARAM_INT);
            $chk_sql->execute();

            if($chk_sql->rowCount() > 0){

            	$row = $chk_sql->fetch(PDO::FETCH_ASSOC);
            	$user_ids = $row['user_id'];
            	$ownerids = $row['ownerid'];
            	
            	$pieces = explode(",", $user_ids);
                $key=array_search($userid, $pieces);
                if(FALSE !== $key)
                {
                    unset($pieces[$key]);
                }
                $userids = implode(",", $pieces);
                //echo $userids;die;

            	//$group = str_replace(' ', '$$', $row['groupname']);

            	$owner = explode('@', $ownerids);
                $ownerid = $owner[0];

                $sql1 = "UPDATE ".CUSTOMGROUP." SET `user_id`=? WHERE groupid=?";
                $insert_sql = $this->db->conn_id->prepare($sql1);
                $insert_sql->bindValue(1, $userids, PDO::PARAM_INT);
                $insert_sql->bindValue(2, $groupid, PDO::PARAM_INT);
                $insert_sql->execute();

                $open = $this->creategroupmember($row['groupname'],$groupids,$ownerid,$userids);

                 if ($insert_sql) {

                    $respose = array("userid" => $userids, "groupname" => $row['groupname'], "groupid" => $groupid);

                    $response->removeGroupUser->responseCode=TRANSACTION_CODE_SUCCESS;
					$response->removeGroupUser->responseMessage=MESSAGE_SUCCESS;
					$response->removeGroupUser->responseData=$respose;
                } else {

                    $response->removeGroupUser->responseCode=TRANSACTION_CODE_FAILED;
					$response->removeGroupUser->responseMessage="User not added.";
                }

            }else{
            	$response->removeGroupUser->responseCode=TRANSACTION_CODE_FAILED;
				$response->removeGroupUser->responseMessage="No Group found.";
            }

		}else{
		$response->addGroupUser->responseCode=TRANSACTION_CODE_FAILED;
		$response->addGroupUser->responseMessage="No request data found.";
	}
	return $response;
	}


	function creategroupmember($group,$groupids,$ownerid,$userids){
		$owner=$ownerid;
		$naturalNames=$group;
		$naturalName=str_replace('$$',' ',$naturalNames);
		$roomName=$groupids;
		$description="ICN PRIVATE";
		$users1=$userids;
		//echo $users1;
		$users=str_replace(' ','',$users1);
		$user=explode(',',$users);
		$var="";
		foreach($user as $us)
		{
		 $var.="<member>".$us."</member>";
		}

		    $url = OPENFIRE_SERVER."plugins/mucservice/chatrooms?servicename=conference";
		    $data = "<chatRoom>
		                <roomName>$roomName</roomName>
		                <naturalName>$naturalName</naturalName>
		                <description>$description</description>
		                <publicRoom>false</publicRoom>
		                <persistent>true</persistent>   
		                <canOccupantsInvite>true</canOccupantsInvite>               
		                <owners>
		                    <owner>$owner@conference.trinityapplab.in</owner>
		                </owners>
		                <members>
		                    $var
		                </members>
		            </chatRoom>";

		    $openfire_username = OPENFIRE_USERNAME;
		    $openfire_password = OPENFIRE_PASSWORD;

		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_PORT, "9090");
		    curl_setopt($ch, CURLOPT_POST, 1);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		    curl_setopt($ch, CURLOPT_HTTPHEADER,
		                     array('Content-Type: application/xml',
		                           'Authorization: Basic '.base64_encode("$openfire_username:$openfire_password")));
		    
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    $res = curl_exec($ch);
		    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    //---------------------
		    $responseInfo    = curl_getinfo($ch);
		    $header_size    = curl_getinfo($ch, CURLINFO_HEADER_SIZE); 
		    $responseHeader = substr($res, 0, $header_size);
		    $responseBody   = substr($res, $header_size);
		 
		    curl_close($ch);
		    return $code;
	}


	function wayUpdate($requestinfo){
		$response=getNewObject();
		$response->wayUpdate=getNewObject();

		if(checkExists($requestinfo)){

			$group_id = isset($requestinfo->group_id)?$requestinfo->group_id:'';
			$way = isset($requestinfo->way)?$requestinfo->way:'';

            $sql = "SELECT g_id FROM ".CUSTOMGROUP." WHERE groupid=?";
            $chk_sql = $this->db->conn_id->prepare($sql);
            $chk_sql->bindValue(1, $group_id, PDO::PARAM_INT);
            $chk_sql->execute();

            if($chk_sql->rowCount() > 0){

                $sql1 = "UPDATE ".CUSTOMGROUP." SET `way`=? WHERE groupid=?";
                $update_sql = $this->db->conn_id->prepare($sql1);
                $update_sql->bindValue(1, $way, PDO::PARAM_INT);
                $update_sql->bindValue(2, $group_id, PDO::PARAM_INT);
                $update_sql->execute();

                 if ($update_sql) {

                    $respose = array("groupid" => $group_id, "way"=>$way);

                    $response->wayUpdate->responseCode=TRANSACTION_CODE_SUCCESS;
					$response->wayUpdate->responseMessage=MESSAGE_SUCCESS;
					$response->wayUpdate->responseData=$respose;
                } else {

                    $response->wayUpdate->responseCode=TRANSACTION_CODE_FAILED;
					$response->wayUpdate->responseMessage=MESSAGE_FAILED;
                }

            }else{
            	$response->wayUpdate->responseCode=TRANSACTION_CODE_FAILED;
				$response->wayUpdate->responseMessage="No Group found.";
            }

		}else{
		$response->wayUpdate->responseCode=TRANSACTION_CODE_FAILED;
		$response->wayUpdate->responseMessage="No request data found.";
	}
	return $response;
	}

	function checkWay($requestinfo){
		$response=getNewObject();
		$response->checkWay=getNewObject();

		if(checkExists($requestinfo)){

			$group_id = isset($requestinfo->group_id)?$requestinfo->group_id:'';

            $sql = "SELECT way FROM ".CUSTOMGROUP." WHERE groupid=?";
            $chk_sql = $this->db->conn_id->prepare($sql);
            $chk_sql->bindValue(1, $group_id, PDO::PARAM_INT);
            $chk_sql->execute();

            if($chk_sql->rowCount() > 0){

                $row = $chk_sql->fetch(PDO::FETCH_ASSOC);
                $way = $row['way'];

                $respose = array("groupid" => $group_id, "way"=>$way);

                $response->checkWay->responseCode=TRANSACTION_CODE_SUCCESS;
				$response->checkWay->responseMessage=MESSAGE_SUCCESS;
				$response->checkWay->responseData=$respose;

            }else{
            	$response->checkWay->responseCode=TRANSACTION_CODE_FAILED;
				$response->checkWay->responseMessage="No Group found.";
            }

		}else{
		$response->checkWay->responseCode=TRANSACTION_CODE_FAILED;
		$response->checkWay->responseMessage="No request data found.";
	}
	return $response;
	}


	function sendMailToOwner($requestinfo){
		$response=getNewObject();
		$response->sendMailToOwner=getNewObject();

		if(checkExists($requestinfo)){

			if($requestinfo->owner_mail!=''){
				$group_code = isset($requestinfo->group_code)?$requestinfo->group_code:'';
				$owner_mail = isset($requestinfo->owner_mail)?$requestinfo->owner_mail:'';
				$owner_id = isset($requestinfo->owner_id)?$requestinfo->owner_id:'';

	            $owner_qry = "SELECT id FROM ".USERLIST." where user_id = ?";
	            $owner_sql = $this->db->conn_id->prepare($owner_qry);
	            $owner_sql->bindValue(1, $owner_id, PDO::PARAM_INT);
	            $owner_sql->execute();

	            if($owner_sql->rowCount() > 0){
	            	
	            	$sql1 = "UPDATE ".USERLIST." SET `email`=? WHERE user_id=?";
	                $update_sql = $this->db->conn_id->prepare($sql1);
	                $update_sql->bindValue(1, $owner_mail, PDO::PARAM_INT);
	                $update_sql->bindValue(2, $owner_id, PDO::PARAM_INT);
	                $update_sql->execute();

	            	$email = 'arpit.aggarwal@trinityapplab.co.in';
	                $subject = "Unique Group ID";
	                $message = "Unique Group ID is ".$group_code;
	                $headers  = 'MIME-Version: 1.0' . "\r\n";
	                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	                $headers .= 'From: '.$email. "\r\n";
	                $mail = mail($owner_mail, $subject, $message, $headers);

	                if($mail){
	                	$response->sendMailToOwner->responseCode=TRANSACTION_CODE_SUCCESS;
						$response->sendMailToOwner->responseMessage=MESSAGE_SUCCESS;
	                }else{
	                	$response->sendMailToOwner->responseCode=TRANSACTION_CODE_FAILED;
						$response->sendMailToOwner->responseMessage=MESSAGE_FAILED;
	                }
	            }else{
	            	$response->sendMailToOwner->responseCode=TRANSACTION_CODE_FAILED;
					$response->sendMailToOwner->responseMessage="No Owner found.";
	            }

			}else{
				$response->sendMailToOwner->responseCode=TRANSACTION_CODE_FAILED;
				$response->sendMailToOwner->responseMessage="Owner mailID is blank.";
			}

		}else{
		$response->sendMailToOwner->responseCode=TRANSACTION_CODE_FAILED;
		$response->sendMailToOwner->responseMessage="No request data found.";
	}
	return $response;
	}


	function saveFiles($requestinfo) {
        $response=getNewObject();
		$response->saveFiles=getNewObject();

        if (checkExists($requestinfo)) {

            $file = $requestinfo->file;
            $filetype = $requestinfo->filetype;
            $to_id = $requestinfo->to_id;

            $FileName='';
			if(isset($file) && ($file)!=''){
			$thefile = base64_decode($file);
			$FileName=$to_id.time().".".$filetype;
			file_put_contents(FILE_URL.$FileName, $thefile);

			$sql = "INSERT INTO `saveFiles`(`to_id`, `filename`, `file_url`) VALUES (?,?,?)";
			$insert_sql = $this->db->conn_id->prepare($sql);
			$insert_sql->bindValue(1, $to_id, PDO::PARAM_INT);
			$insert_sql->bindValue(2, $FileName, PDO::PARAM_INT);
			$insert_sql->bindValue(3, FILE_SAVE_URL.$FileName, PDO::PARAM_INT);
			$insert_sql->execute();

			if($insert_sql){
				$response->saveFiles->responseCode=TRANSACTION_CODE_SUCCESS;
				$response->saveFiles->responseMessage=MESSAGE_SUCCESS;
				$response->saveFiles->filename=$FileName;
			}else{
				$response->saveFiles->responseCode=TRANSACTION_CODE_FAILED;
				$response->saveFiles->responseMessage=MESSAGE_FAILED;
			}
			}else{
				$response->saveFiles->responseCode=TRANSACTION_CODE_FAILED;
				$response->saveFiles->responseMessage=MESSAGE_FAILED;
			}
      
        } else {
            $response->saveFiles->responseCode=TRANSACTION_CODE_FAILED;
			$response->saveFiles->responseMessage="No request data found.";
        }
        return $response;
    }


    function searchUser($requestinfo) {
        $response=getNewObject();
		$response->searchUser=getNewObject();

        if (checkExists($requestinfo)) {

            $searchstring = $requestinfo->searchstring;
            $user_array = array();

			if($searchstring!=''){
				$sql = "(SELECT `name`, `mobilenumber`, `user_id`, `email`, `image`, `type` FROM ".USERLIST." WHERE `name` like '%$searchstring%') UNION (SELECT u.`name`, u.`mobilenumber`, u.`user_id`, u.`email`, u.`image`, u.`type` FROM ".CHILDLIST." c, ".USERLIST." u WHERE c.user_id=u.user_id and c.`childname` like '%$searchstring%')";
				$user_sql = $this->db->conn_id->prepare($sql);
				$user_sql->execute();
				if($user_sql->rowCount() >0){
					while($row =$user_sql->fetch(PDO::FETCH_ASSOC)){
						//print_r($row);die;
						$child_array = array();
						$sql1 = "SELECT * FROM ".CHILDLIST." WHERE `user_id`=?";
							$child_sql = $this->db->conn_id->prepare($sql1);
							$child_sql->bindValue(1, $row['user_id'], PDO::PARAM_INT);
							$child_sql->execute();
							
							if($child_sql->rowCount()>0){
								while($child_row = $child_sql->fetch(PDO::FETCH_ASSOC)){
									$child_array[] = array("Name"=>$child_row['childname'], "schoolID"=>$child_row['school_id'], "teamCode"=>$child_row['team_code']);
								}
							}

						$user_array[] = array("User_id"=>$row['user_id'], "Name"=>$row['name'], "Mobile"=>$row['mobilenumber'], "Email"=>$row['email'], "Image"=>$row['image'], "type"=>$row['type'], "childInfo"=>$child_array);
					}
				}
				
			}else{
				$sql = "SELECT `name`, `mobilenumber`, `user_id`, `email`, `image`, `type` FROM ".USERLIST." WHERE `user_id`!='' and `type`!='S'";
				$user_sql = $this->db->conn_id->prepare($sql);
				$user_sql->execute();
				if($user_sql->rowCount() > 0){
					while($row = $user_sql->fetch(PDO::FETCH_ASSOC)){
						$child_array = array();
						$sql1 = "SELECT * FROM ".CHILDLIST." WHERE `user_id`=?";
							$child_sql = $this->db->conn_id->prepare($sql1);
							$child_sql->bindValue(1, $row['user_id'], PDO::PARAM_INT);
							$child_sql->execute();
							
							if($child_sql->rowCount()>0){
								while($child_row = $child_sql->fetch(PDO::FETCH_ASSOC)){
									$child_array[] = array("Name"=>$child_row['childname'], "schoolID"=>$child_row['school_id'], "teamCode"=>$child_row['team_code']);
								}
							}

						$user_array[] = array("User_id"=>$row['user_id'], "Name"=>$row['name'], "Mobile"=>$row['mobilenumber'], "Email"=>$row['email'], "Image"=>$row['image'], "type"=>$row['type'], "childInfo"=>$child_array);
					}
				}
			}

			$response->searchUser->responseCode=TRANSACTION_CODE_SUCCESS;
			$response->searchUser->responseMessage=MESSAGE_SUCCESS;
			$response->searchUser->responseDataArray=$user_array;
      
        } else {
            $response->searchUser->responseCode=TRANSACTION_CODE_FAILED;
			$response->searchUser->responseMessage="No request data found.";
        }
        return $response;
    }


}

