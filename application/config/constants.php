<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */

define("DOCUMENT_ROOT",$_SERVER['DOCUMENT_ROOT']);
define("HOST_PATH",'http://'.$_SERVER['HTTP_HOST']);
define('IMAGE_PATH',$_SERVER['DOCUMENT_ROOT'].'/Registrations');


define('MULTIPLY','12');
define("PREFIX",'_');



//web page contants
//define("MENU",HOSTNAME."reasonable_genius/adminmenu");
//define("LOGOUT",SITE_PATH."/logout");

if(in_array($_SERVER['HTTP_HOST'],array('192.168.1.14','localhost'))){
	define('HOST','localhost');
	define('DB_NAME','ICNNEW');
	define('DB_USERNAME','root');
	define('DB_PASSWORD','tr!n!ty@pp1@b');
	define("MAIL_PATH","C:/sendmail/sendmail.exe -t");
	define("IS_LOCAL",TRUE);
}
else{
	define('HOST','localhost');// host
	define('DB_NAME','ICNNEW');//db name
	define('DB_USERNAME','root');// server username
	define('DB_PASSWORD','tr!n!ty@pp1@b');// server password
	define("MAIL_PATH","/usr/sbin/sendmail -t -i"); //server mail path here
	define("IS_LOCAL",FALSE);
}

define('NUMBER', 'fdfdfdsfdsfdsfdf5445878878844444');

//table name constants//
define("CATEGORY","category");
define("CATEGORYCAPTION","category_caption");
define("CHILDLIST","childlist");
define("USERLIST","userlist");
define("CUSTOMGROUP","customgroup");
define("LOGIC","logic");    
define("OTPLOG","otp_log");
define("SCHOOLLIST","schoollists");
define("SUPPORT","support");
define("USERDEVICE","user_device");


define("TRANSACTION_CODE_FAILED",204);
define("MESSAGE_FAILED","FAILURE");
define("TRANSACTION_CODE_SUCCESS",200);
define("MESSAGE_SUCCESS","SUCCESS");
define("TRANSACTION_CODE_INVALID",417);
define("TRANSACTION_CODE_UNAUTH",401);
define("TRANSACTION_MSG_UNAUTH","Unauthorized access.");
define("TRANSACTION_MSG_FAILED","Transaction failed.");
define("TRANSACTION_CODE_FORBIDDEN",403);
define("TRANSACTION_MSG_FORBIDDEN","Forbidden access.");
define("TRANSACTION_CODE_SESSIONOUT",203);
define("TRANSACTION_MSG_SESSIONOUT","Session expired, please login again.");
define("TRANSACTION_CODE_NO_RECORD",100);
define("TRANSACTION_MSG_NO_RECORD","No Record Found!");


define("IMAGE_SAVE_URL","http://".$_SERVER['HTTP_HOST']."/ICNProd/upload/");
define("IMAGE_URL",$_SERVER['DOCUMENT_ROOT']."/ICNProd/upload/");

define("OPENFIRE_SERVER","http://www.trinityapplab.in:9090/");
define("OPENFIRE_USERNAME","admin");
define("OPENFIRE_PASSWORD","trinity@123");


define("FILE_SAVE_URL","http://".$_SERVER['HTTP_HOST']."/ICNProd/upload/");
define("FILE_URL",$_SERVER['DOCUMENT_ROOT']."/ICNProd/upload/");





/* Location: ./application/config/constants.php */
